
build: tmp var
	mkdir -p build
	cp packages/freetds-stable.tgz tmp/
	cd tmp/ && tar xf freetds-stable.tgz
	cd tmp/freetds* && ([ ! -f config.status ] && ./configure --disable-odbc --prefix=$(CURDIR)/$@) || true
	cd tmp/freetds* &&  make install

tmp:
	mkdir $(CURDIR)/tmp

var:
	mkdir -p $(CURDIR)/var/log

clean:
	-@rm -rf $(CURDIR)/build
	-@rm -rf $(CURDIR)/tmp
	-@rm -rf $(CURDIR)/var
