SELECT t.text, s.* FROM(
SELECT 
  COUNT(8) [Handle Row Count],
  SUM(stats.execution_count) [Execution Count],
  AVG(stats.min_rows) [AVG Min Rows],
  SUM(stats.total_elapsed_time)/1000 [Total Time (ms)],
  stats.sql_handle 
FROM 
  sys.dm_exec_query_stats stats
GROUP BY stats.sql_handle
) s
CROSS APPLY 
  sys.dm_exec_sql_text( s.sql_handle ) t
ORDER BY s.[Total Time (ms)] DESC
