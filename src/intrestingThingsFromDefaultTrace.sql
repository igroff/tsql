declare @TEMPDB_ID as int
select @TEMPDB_ID=DB_ID('tempdb')
SELECT StartTime
       , f.LoginName
     , login.name [TargetLoginName]
     , f.EventClass
     , f.DatabaseName
     , f.ObjectName
     , OBJECT_NAME(f.ObjectID, f.SourceDatabaseID) [ObjectName]
     , OBJECT_NAME(f.ObjectID2, f.SourceDatabaseID) [Object2Name]
     , f.ObjectID
     , f.ObjectType
       , f.*
FROM   sys.traces t
       CROSS APPLY fn_trace_gettable(REVERSE(SUBSTRING(REVERSE(t.path),CHARINDEX('\', REVERSE(t.path)),260)) + N'log.trc', DEFAULT) f
     LEFT OUTER JOIN syslogins login on cast(login.sid as varbinary(85)) = cast(f.TargetLoginSid as varbinary(85))
WHERE  
  t.is_default = 1 -- only looking for the default trace
  AND f.DatabaseName != 'GLG_SEARCH'
  AND EventClass in(
    14    /* audit login */
    , 15  /* audit logout */
    , 20  /* audit login failed */
    , 17    /* existing connection */
    , 102
    , 103
    , 104
    , 105
    , 106
    , 107
    , 108
    , 109
    , 110
    , 111
    , 112
    , 128 /* audit database management event */
    , 129   /* audit database object management event */
    , 130   /* audit database principal management event */
    , 131   /* audit schema object management event */
    , 132   /* audit server principal impersonation event */
    , 133   /* audit database principal impersonation event */
    , 134   /* audit server object take ownership event */
    , 135   /* audit database object take ownership event */
    , 152   /* audit change database owner */
    , 153   /* audit schema object take ownership event */
    , 162   /* user error message */
    , 164   /* object altered */
  )/*
  AND EventClass NOT IN (
    115 /* backup audit */
    , 55 /* hash warning */
    , 69 /* sort warning */
    , 116 /* dbcc command */
    , 46  /* object created */
    , 47  /* object deleted */
    , 175 /* alter trace */
  )*/
  AND f.DatabaseID != @TEMPDB_ID


                          

