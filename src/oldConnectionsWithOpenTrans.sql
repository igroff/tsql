SELECT 
  DATEDIFF(ss, last_batch, GETDATE()) [Age (s)], 
  spid, 
  blocked, 
  t.request_mode [Lock Type],
  DB_NAME(s.dbid) AS DBNAME,
  s.status,
  s.open_tran,
  s.login_time,
  s.program_name,
  LEFT(stxt.text, 80) AS SQLSTATEMENT,
  *
FROM master..sysprocesses s 
INNER JOIN sys.dm_tran_locks t on t.request_session_id = s.spid
CROSS APPLY sys.dm_exec_sql_text(s.sql_handle) stxt
WHERE 
open_tran > 0 
AND last_batch < DATEADD(ss, -120, GETDATE())

