USE master;
GO

IF EXISTS (SELECT * FROM sysobjects WHERE type ='p' and name= 'sp_ilich')
BEGIN
  DROP PROCEDURE sp_ilich
END
GO

CREATE PROCEDURE sp_ilich
  @really_kill_em AS BIT = 0
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  DECLARE @kill_spids AS BIT

  SELECT @kill_spids = @really_kill_em
  --    just being helpful, tell you where we're looking
  PRINT 'Looking for evil sleepers in ' + DB_NAME(DB_ID())

  DECLARE dead_spids_cursor CURSOR FOR SELECT DISTINCT S.spid
  FROM sys.dm_tran_locks (nolock) L
       JOIN sys.sysprocesses (nolock) S on S.spid = L.request_session_id
       JOIN sys.partitions (nolock) P ON P.hobt_id = L.resource_associated_entity_id
       JOIN sys.objects (nolock) O ON O.object_id = P.object_id
       JOIN sys.dm_exec_sessions (nolock) ES ON ES.session_id = L.request_session_id
       JOIN sys.dm_tran_session_transactions (nolock) TST ON ES.session_id = TST.session_id
       JOIN sys.dm_tran_active_transactions (nolock) AT ON TST.transaction_id = AT.transaction_id
       JOIN sys.dm_exec_connections (nolock) CN ON CN.session_id = ES.session_id
  WHERE
         resource_database_id = db_id()
         AND S.open_tran > 0
         AND S.status = 'sleeping'
         AND ES.last_request_end_time < DATEADD(ss, -12, GETDATE())
         AND L.request_mode like '%X%' -- lookin' for exclusive locks
    
  DECLARE @dead_spid AS INT
  DECLARE @kill_statement AS NVARCHAR(255)
  DECLARE @log_statement AS NVARCHAR(255)
  DECLARE @done_killed_some AS BIT = 0

  IF ( 0 = @kill_spids )
  BEGIN
    PRINT 'You''ve asked me NOT to kill, I''ll only show you what I would kill.'
  END  
  OPEN dead_spids_cursor

  FETCH NEXT FROM dead_spids_cursor INTO @dead_spid
  WHILE @@FETCH_STATUS = 0
  BEGIN

    SELECT @kill_statement = N'KILL ' + CAST(@dead_spid AS CHAR(5))
    SELECT @log_statement = N'Killing sleeping SPID with: ' + @kill_statement
    IF ( 1 = @kill_spids )
    BEGIN
      SET @done_killed_some = 1
      EXEC sp_executesql @kill_statement
      --    only log it if we do it
      EXEC xp_logevent 501212, @log_statement
      --    I'd like to see what this just did if I'm running it interactively
      PRINT @log_statement
    END 
    ELSE
    BEGIN
      --    show the statements we _would_ run
      PRINT @kill_statement
    END

    FETCH NEXT FROM dead_spids_cursor INTO @dead_spid 
  END

  CLOSE dead_spids_cursor
  DEALLOCATE dead_spids_cursor

  IF ( 0 = @done_killed_some )
  BEGIN
    PRINT 'No sleeping spids found to kill'
  END
END
GO


