USE TESTDB
GO
SET NOCOUNT ON
if exists (select 1 from sysobjects where type='u' and name='test_join_table') 
begin
  drop table test_join_table
end
if exists (select 1 from sysobjects where type='u' and name='test_table_1') 
begin
  drop table test_table_1
end
if exists (select 1 from sysobjects where type='u' and name='test_table_2') 
begin
  drop table test_table_2
end


GO
create table test_table_1 (
  id int identity primary key,
  an_int int,
  name varchar(25)
)

create table test_table_2(
  id int identity primary key,
  an_int int,
  name varchar(25)
)

create table test_join_table(
  table_1_id int not null references test_table_1(id),
  table_2_id int not null references test_table_2(id),
  why varchar(255)
)
GO
--******************************************************************************** 
delete from test_join_table
delete from test_table_1
delete from test_table_2
go
begin tran
insert into test_table_1 (an_int, name) values (1, 'second')
insert into test_table_2 (an_int, name) values ('not an int', 'second')
commit tran
go

declare @count int
select @count = count(*) from test_table_1
if (@count = 0)
begin
  print '****************************************************************'
  print 'based on the previous example you might expect this not to print, but it did'
  print '****************************************************************'
end
go

--******************************************************************************** 
