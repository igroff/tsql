USE TESTDB
GO
SET NOCOUNT ON
if exists (select 1 from sysobjects where type='u' and name='test_join_table') 
begin
  drop table test_join_table
end
if exists (select 1 from sysobjects where type='u' and name='test_table_1') 
begin
  drop table test_table_1
end
if exists (select 1 from sysobjects where type='u' and name='test_table_2') 
begin
  drop table test_table_2
end

GO
create table test_table_1 (
  id int identity primary key,
  an_int int,
  name varchar(25)
)

create table test_table_2(
  id int identity primary key,
  an_int int,
  name varchar(25)
)

create table test_join_table(
  table_1_id int not null references test_table_1(id),
  table_2_id int not null references test_table_2(id),
  why varchar(255)
)
GO

begin tran
insert into test_table_1 (an_int, name) values (1, 'first')
insert into test_table_2 (an_int, name) values (1, 'second')
-- this will fail because we do not have a id of 4 in table 2
insert into test_join_table (table_1_id, table_2_id, why) values (1,4, 'cuz')
commit tran
go
declare @name1 as varchar(25)
declare @name2 as varchar(25)
select @name1 = name from test_table_1 where id = 1
select @name2 = name from test_table_2 where id = 1
if (@name1 = 'first' and @name2 = 'second') 
begin
  print '****************************************************************'
  print '  OH CRAP, two of those committed!!!'
  print '****************************************************************'
end
go
