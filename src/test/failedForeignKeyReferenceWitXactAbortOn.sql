USE TESTDB
GO
SET NOCOUNT ON
if exists (select 1 from sysobjects where type='u' and name='test_join_table') 
begin
  drop table test_join_table
end
if exists (select 1 from sysobjects where type='u' and name='test_table_1') 
begin
  drop table test_table_1
end
if exists (select 1 from sysobjects where type='u' and name='test_table_2') 
begin
  drop table test_table_2
end


GO
create table test_table_1 (
  id int identity primary key,
  an_int int,
  name varchar(25)
)

create table test_table_2(
  id int identity primary key,
  an_int int,
  name varchar(25)
)

create table test_join_table(
  table_1_id int not null references test_table_1(id),
  table_2_id int not null references test_table_2(id),
  why varchar(255)
)

GO
set xact_abort on
begin tran
insert into test_table_1 (an_int, name) values (1, 'first')
insert into test_table_2 (an_int, name) values (1, 'second')
-- this will fail because we do not have a id of 4 in table 2
insert into test_join_table (table_1_id, table_2_id, why) values (1,4, 'cuz')
commit tran
go
declare @table_1_count as int
declare @table_2_count as int
declare @join_table_count as int
select @table_1_count = count(*) from test_table_1
select @table_2_count = count(*) from test_table_2
select @join_table_count = count(*) from test_join_table
if (@table_1_count = 0 and @table_2_count = 0) 
begin
  print '****************************************************************'
  print '  Whew, none of those inserts took.  It''s like auto rollback man.'
  print '****************************************************************'
end
go
